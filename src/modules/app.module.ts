import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController, OrderController, ItemController } from '../controllers'
import { AppService, ItemService, OrderService } from '../services'
import { Item, Order, ItemSchema, OrderSchema } from '../schemas'


@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/demo'),
    MongooseModule.forFeature([
      { name: Item.name, schema: ItemSchema },
      { name: Order.name, schema: OrderSchema },
    ]),
  ],
  controllers: [AppController, OrderController, ItemController],
  providers: [AppService, OrderService, ItemService],
})
export class AppModule { }
