import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ItemDocument = Item & Document;

@Schema()
export class Item {
  @Prop()
  order_id: string;

  @Prop()
  name: string;

  @Prop()
  sku: string;

  @Prop()
  quantity: number;

  @Prop()
  price: number;
}

export const ItemSchema = SchemaFactory.createForClass(Item);
