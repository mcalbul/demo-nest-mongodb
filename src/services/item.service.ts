import { Item, ItemDocument } from '@app/schemas/item.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ItemService {
  constructor(@InjectModel(Item.name) private itemModel: Model<ItemDocument>) { }

  async get(): Promise<Item[]> {
    return await this.itemModel.find().exec();
  }
}
