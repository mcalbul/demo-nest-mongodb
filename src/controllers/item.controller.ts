import { Controller, Get } from "@nestjs/common";
import { Item } from "@app/schemas/item.schema";
import { ItemService } from "@app/services/item.service";

@Controller('item')
export class ItemController {
  constructor(private readonly itemService: ItemService) { }

  @Get()
  async get(): Promise<Item[]> {
    return await this.itemService.get();
  }
}