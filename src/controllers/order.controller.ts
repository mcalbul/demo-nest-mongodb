import { Controller, Get } from '@nestjs/common';
import { Order } from '@app/schemas/order.schema';
import { OrderService } from '@app/services/order.service';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) { }

  @Get()
  async get(): Promise<Order[]> {
    return await this.orderService.get();
  }
}
